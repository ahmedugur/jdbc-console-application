import org.mindrot.jbcrypt.BCrypt;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class Repository {
    Util util = new Util();

    public ArrayList<Person> view() {
        ArrayList<Person> personList = new ArrayList();
        try (Connection connection = util.getConnection();
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM person")) {
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Person person = new Person();
                person.setId(resultSet.getInt("id"));
                person.setName(resultSet.getString("name"));
                person.setSurName(resultSet.getString("surname"));
                person.setAge(resultSet.getInt("age"));
                person.setPhone(resultSet.getString("phonenumber"));
                personList.add(person);
            }


        } catch (SQLException e) {
            e.printStackTrace();
        }
        return personList;
    }

    public int savePerson(Person person) {
        int id = person.getId();
        String password = person.getPassword();
        String hashed = BCrypt.hashpw(password, BCrypt.gensalt(12));
        try (Connection connection = util.getConnection();

             PreparedStatement statement = connection.prepareStatement("INSERT INTO person (name,surname,age,username,password,phonenumber,money) values (?, ?, ?,?,?,?,?)")) {
            statement.setString(1, person.getName());
            statement.setString(2, person.getSurName());
            statement.setString(3, String.valueOf(person.getAge()));
            statement.setString(4, person.getUserName());
            statement.setString(5, hashed);
            statement.setString(6, person.getPhone());
            statement.setInt(7,person.getMoney());
            statement.executeUpdate();
            PreparedStatement statement1 = connection.prepareStatement("select id from  person where username=?");
            statement1.setString(1,person.getUserName());
            ResultSet resultSet = statement1.executeQuery();
            while (resultSet.next()){

               id = resultSet.getInt("id");
            }


        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id;
    }

    public void delete(int id) {
        try (Connection connection = util.getConnection();
             PreparedStatement statement = connection.prepareStatement("delete from person where id=?")) {
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void update(int id, String name, String surname, String age) {
        try (Connection connection = util.getConnection();
             PreparedStatement statement = connection.prepareStatement("update person set name=?,surname=?,age=? where id=?")) {
            statement.setString(1, name);
            statement.setString(2, surname);
            statement.setString(3, age);
            statement.setInt(4, id);
            statement.executeUpdate();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }


    public boolean checkLogin(String username) {
        try (
                Connection connection = util.getConnection();
                PreparedStatement statement = connection.prepareStatement("select * from person where username=?")) {
            statement.setString(1, username);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                return false;
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return true;

    }




    public ArrayList<Product> viewProduct() {
       ArrayList<Product> productList = new ArrayList();
    try (Connection connection = util.getConnection();
    PreparedStatement statement = connection.prepareStatement("select * from product")){
    ResultSet resultSet = statement.executeQuery();
    while (resultSet.next()){
        Product product = new Product();
        product.setProductName(resultSet.getString("name"));
        product.setQty(resultSet.getInt("quantity"));
        product.setPrice(resultSet.getString("price"));
        productList.add(product);
    }
    }catch (SQLException throwables){
        throwables.printStackTrace();

    }
    return productList;
    }


    public String check(int id,String productName,int personQuantity) {
        int money = 0;
        int price = 0;
        int productQuantity = 0;
        int sum;
        int updatedMoney;
        int updatedQuantity;

        try (Connection connection=util.getConnection();
            PreparedStatement statement = connection.prepareStatement("select money from person where id=?")) {
            statement.setInt(1,id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                money = resultSet.getInt("money");
            }

            PreparedStatement statement1 = connection.prepareStatement("select price from product where name=?");
            statement1.setString(1, productName);
            ResultSet resultSet1 = statement1.executeQuery();
            while (resultSet1.next()){
                price = resultSet1.getInt("price");
            }

            PreparedStatement statement2 = connection.prepareStatement("select quantity from  product where name=?");
            statement2.setString(1,productName);
            ResultSet resultSet2 = statement2.executeQuery();
            while (resultSet2.next()){
                productQuantity = resultSet2.getInt("quantity");
            }

            if (productQuantity<personQuantity){
                System.out.println("Товара не достаточно в наличии");
                Main.selectOperation();
            }

            sum = price * personQuantity;


            if (money<sum){
                System.out.println("У вас не достаточно денег");
                Main.selectOperation();

            }

                updatedMoney = money-sum;
                System.out.println(updatedMoney);
                System.out.println(id);
                PreparedStatement statement3 = connection.prepareStatement("update person set money=? where id=?");
                statement3.setInt(1,updatedMoney);
                statement3.setInt(2,id);
                statement3.executeUpdate();


                updatedQuantity = productQuantity-personQuantity;
                System.out.println(updatedQuantity);
                PreparedStatement statement4 = connection.prepareStatement("update product set quantity=? where name=?");
                statement4.setInt(1,updatedQuantity);
                statement4.setString(2,productName);
                statement4.executeUpdate();


            String messege = "Вы преобрели "+productName+" в колличестве "+personQuantity+" шт";
            return messege;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    return null;
    }
}
