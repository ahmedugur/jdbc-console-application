import org.mindrot.jbcrypt.BCrypt;

import java.util.Objects;

public class Person {
    private String name;
    private String surName;
    private int age;
    private int id;
    private String phone;
    private String userName;
    private String password;
    private int money;


    public int getMoney() {
        return money;
    }


    public void setMoney(int money) {
        this.money = money;
    }


    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", surName='" + surName + '\'' +
                ", age=" + age +
                ", id=" + id +
                ", phone='" + phone + '\'' +
                '}';
    }



    public String getPhone() {
        return phone;
    }







    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurName() {
        return surName;
    }



    public void setSurName(String surName) {
        this.surName = surName;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }

}
