public class Product {
    public String productName;
    public String price;
    public int qty;


    public void setQty(int qty) {
        this.qty = qty;
    }




    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }


    public int getQty() {
        return qty;
    }

    @Override
    public String toString() {
        return "Product{" +
                "productName='" + productName + '\'' +
                ", price='" + price + '\'' +
                ", qty=" + qty +
                '}';
    }


}
