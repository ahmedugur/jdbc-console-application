import javax.swing.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static Repository repository = new Repository();
    public static int id;
    public static void main(String[] args) throws SQLException {
        Util util = new Util();

        Connection connection = util.getConnection();
        Statement statement = connection.createStatement();
        String queryCreateTable = "create table if not exists person1"+
                "(id serial primary key,"+
                "name varchar(255),"+
                "surname varchar(255),"+
                "age varchar(255),"+
                "username text,"+
                "password varchar(255),"+
                "phonenumber varchar(255),"+
                "money integer)";
        statement.execute(queryCreateTable );

        Statement statement1 = connection.createStatement();
        String queryCreateTableProduct = "create table if not exists product1" +
                "( id serial primary key ,"+
                "name varchar(255),"+
                "price integer,"+
                "quantity integer)";
        statement1.execute(queryCreateTableProduct);


        PreparedStatement statement2 = connection.prepareStatement("INSERT INTO product (name,price,quantity) values (?, ?, ?,?,?,?,?)");
        statement2.setString(1,"noutbook");
        statement2.setInt(2,1000);
        statement2.setInt(3,20);
        selectOperation();


    }
    public static void selectOperation() throws SQLException {

        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Выберите операцию:");
            System.out.println("Для ввода данных пользоваьеля введите 1");
            System.out.println("Для вывода данных пользоваля введите 2");
            System.out.println("Для удаления данных пользоваля введите 3");
            System.out.println("Для изменеия данных пользоваля введите 4");
            System.out.println("Для покупки товара введите 5");
            String result = scanner.nextLine();
            switch (result) {
                case "1":
                    Person person = addPerson();
                    id = repository.savePerson(person);
                    break;

                case "2":
                    ArrayList<Person> personList = repository.view();
                    for (Person pers : personList) {
                        System.out.println(pers);

                    }
                    break;
                case "3":
                    System.out.println("Введите индекс пользователя");
                    int idDelete = scanner.nextInt();
                    repository.delete(idDelete);
                    break;
                case "4":
                    System.out.println("Введите индекс пользователя");
                    int idUpdate = Integer.parseInt(scanner.nextLine());
                    System.out.println("Введите имя пользователя");
                    String name = scanner.nextLine();
                    System.out.println("Введите фамилия пользователя");
                    String surname = scanner.nextLine();
                    System.out.println("Введите возраст пользователя");
                    String age = scanner.nextLine();
                    System.out.println("Введите телефно пользователя");
                    repository.update(idUpdate, name, surname, age);
                    break;
                case "5":
                    Product product = new Product();
                    ArrayList<Product> productList = repository.viewProduct();
                    for (Product prod:productList) {
                        System.out.println(prod);

                    }
                    System.out.println("Введиет название товара");
                    product.setProductName(scanner.nextLine());
                    String productName = product.getProductName();
                    System.out.println("Введите количество преобретаемого товара");
                    product.setQty(Integer.parseInt(scanner.nextLine()));
                    int quantity = product.getQty();
                    String message = repository.check(id,productName,quantity);
                    System.out.println(message);
                    break;
                default:
            }
        }
    }

    private static Person  addPerson() {
        Scanner scanner = new Scanner(System.in);
        Person person = new Person();
        System.out.println("Введите имя пользователя");
        person.setName(scanner.nextLine());
        System.out.println("Введите  фамилию пользователя");
        person.setSurName(scanner.nextLine());
        System.out.println("Введите  возраст пользователя");
        person.setAge(Integer.parseInt(scanner.nextLine()));
        while (true){
            System.out.println("Введите телефон пользователя");
            person.setPhone(scanner.nextLine());
            String phone = person.getPhone();
            if (phone.matches("\\+7[0-9]+")){
                break;
            }
            System.out.println("Не соответствует формату, введите телефон в формате +7707");
        }
        while (true) {

            System.out.println("Введите username пользователя");
            person.setUserName(scanner.nextLine());
            String username = person.getUserName();
            if (username.matches("@[a-zA-Z1-9]+")) {
                boolean bol = repository.checkLogin(username);
                if (bol==true){
                    break;
                }
                System.out.println("Пользователь с таким username уже существует");

            }else {

                System.out.println("Не соответствует формату, введите username в формате @username");
            }
        }
        while (true){
            System.out.println("Введите пароль пользователя");
            person.setPassword(scanner.nextLine());
            String password = person.getPassword();
            if (password.matches("[0-9a-z]+")){
                break;
            }
            System.out.println("Не соответствует формату");
        }
        System.out.println("Пополните кошелек на желаемую сумму");
        person.setMoney(Integer.parseInt(scanner.nextLine()));
        return person;
    }
}

