import java.sql.DriverManager;
import java.sql.SQLException;

public class Util {
    String url = "jdbc:postgresql://localhost:5432/postgres";
    String user = "postgres";
    String password = "1";


    public java.sql.Connection getConnection() {
        try {
            Class.forName("org.postgresql.Driver");
            return DriverManager.getConnection(url, user, password);

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }


//    public static Integer toInt(String str){
//        try {
//            Integer.parseInt(str);
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        return null;
//    }
}
